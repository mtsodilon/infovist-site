const nodemailer = require("nodemailer");

export default async (req, res) => {

  const { name, email, phone, company } = req.body.fields;

  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: process.env.infovEmail,
      pass: process.env.infovEmailPassword,
    },
  });

  const htmlEmailBody = `
    <p>Contato enviado via formulário do site Infovist:</p>
    <p><b>Nome:</b> ${name}</p>
    <p><b>E-mail:</b> ${email}</p>
    <p><b>Telefone:</b> ${phone}</p>
    <p><b>Empresa:</b> ${company}</p>
  `;

  const mailOption = {
    from: 'atendimento@infovist.com.br',
    to: 'atendimento@infocar.com.br',
    subject: "Contato via Site Infovist",
    text: `Contato enviado por ${name} (${email})`,
    html: htmlEmailBody,
  };

  transporter.sendMail(mailOption, (err, data) => {
    if (err) {
      res.send("error" + JSON.stringify(err));
    } else {
      res.send("success");
    }
  });

  res.send("success");
};