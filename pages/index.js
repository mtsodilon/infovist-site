import Banner from '../src/components/Banner'
import Benefits from '../src/components/Benefits'
import Contact from '../src/components/Contact'
import DataReceive from '../src/components/DataReceive'
import Footer from '../src/components/Footer'
import Header from '../src/components/Header'
import Operation from '../src/components/Operation'
import Partnership from '../src/components/Partnership'

export default function Home(props) {
  return (
    <>
      <title>Infovist</title>
      <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
      <Header />
      <Banner />
      <Operation />
      <Benefits />
      <DataReceive />
      <Partnership />
      <Contact />
      <Footer />
    </>
  )
}

export const getServerSideProps = async () => {
  return { 
    props: {},
  }
};
