import styled from 'styled-components';

export const Container = styled.div`
  padding: 0px 40px;
  position: relative;

  @media screen and (max-width: 767px) {
    padding: 0;
  }
`;

export const Content = styled.div`
  position: relative
  
  width: 100%;
  height: 100%;
  border-radius: 0 0 30px 30px;
  background-image: url(images/banner.png);
  background-size: cover;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 767px) {
    border-radius: 0;
  }
`;

export const DescriptionWrapper = styled.div`
  width: 65%;
  padding-left: 80px;
  border: 1px solid transparent;

  @media screen and (max-width: 767px) {
    padding: 40px;
    width: 100%;
  }
`;

export const Title = styled.div`
  font-family: Poppins-Bold;
  font-size: 3.5vw;
  line-height: 135%;
  color: #FFFFFF;
  margin-top: 200px;
  z-index: 9;
  position: sticky;

  @media screen and (max-width: 767px) {
    font-size: 40px;
  }
`;

export const Action = styled.button`
  font-family: IBMPlexSansBold;
  padding: 15px 50px;
  background: #D40020;
  border: 1.70213px solid #D40020;
  border-radius: 3.40426px;
  font-weight: bold;
  font-size: 18.7234px;
  line-height: 18px;
  text-align: center;
  color: #FFFFFF;
  margin-bottom: 65px;
  margin-top: 50px;
  cursor: pointer;
  z-index: 9;
  &:hover {
    opacity: 0.9;
  }

  @media screen and (max-width: 767px) {
    width: 100%;
  }
`;

export const DemonstratioWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: flex-start;
  max-width: 40%;
`;

export const DemonstrationImage = styled.img`
  width: 100%;
  z-index: 9;
`;

export const BackgroundLines = styled.div`
  border: 1px solid #DD334D;
  border-top: 0;
  border-bottom-left-radius: 15.0125px;
  border-bottom-right-radius: 15.0125px;
  left: 250px;
  top: 0;
  width: 60%;
  height: 65%;
  position: absolute;

  @media screen and (max-width: 767px) {
    display: none;
  }
`;

export const BackgroundSecondLine = styled.div`
  border-left: 1px solid #DD334D;
  border-bottom: 1px solid #DD334D;
  border-bottom-left-radius: 15.0125px;
  left: 450px;
  right: 40px;
  top: 0;
  height: 85%;
  position: absolute;

  @media screen and (max-width: 767px) {
    display: none;
  }
`;