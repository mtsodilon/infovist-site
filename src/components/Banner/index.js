import styles from '../../../styles/Home.module.css';
import {
  Container,
  Content,
  Title,
  Action,
  DescriptionWrapper,
  DemonstratioWrapper,
  DemonstrationImage,
  BackgroundLines,
  BackgroundSecondLine,
} from "./Banner.styles";

export default function Banner() {
  return (
    <>
      <Container>
        <BackgroundLines />
        <BackgroundSecondLine />
        <Content src="" alt="Banner Infovist">
          <DescriptionWrapper>
            <Title>O app para revolucionar<br />seu processo de vistorias!</Title>
            <Action>Saiba mais</Action>
          </DescriptionWrapper>
          <DemonstratioWrapper className={styles.hiddeOnMobile}>
            <DemonstrationImage
              src="images/banner-app-demonstration.svg"
              alt="Demonstração do App"
            />
          </DemonstratioWrapper>
        </Content>
      </Container>
    </>
  );
}