import styled from 'styled-components';

export const Container = styled.div`
  scroll-behavior: smooth;
  text-align: center;
  padding-top: 130px;
  padding-bottom: 200px;
  position: relative;
  background-image: url(images/rectangle-bg.svg);
  background-size: cover;
`;

export const Background = styled.div`
  margin: 0 0;
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: -9;
`;

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 767px) {
   margin-bottom: 50px;
  }
`;

export const BenefitsItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  text-align: left;

  @media screen and (max-width: 767px) {
    flex-direction: column;
    text-align: center;
  }
`;

export const BenefitsItemRight = styled.div`
  display: flex;
  flex-direction: row-reverse;
  align-items: center;

  text-align: left;

  @media screen and (max-width: 767px) {
    flex-direction: column;
    text-align: center;
  }
`;

export const BenefitsRightPhotoWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;

  @media screen and (max-width: 767px) {
    width: 100vw;
  }
`;

export const BenefitsLeftPhotoWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-start;

  @media screen and (max-width: 767px) {
    width: 100vw;
  }
`;
  
export const ItemPhotoRight = styled.img`
  border-bottom-left-radius: 50px;
  border-top-left-radius: 50px;
  @media screen and (max-width: 767px) {
    width: 95vw;
    right: 0;
  }
`;

export const ItemPhotoLeft = styled.img`
  border-bottom-right-radius: 50px;
  border-top-right-radius: 50px;

  @media screen and (max-width: 767px) {
    width: 95vw;
  }
`;


export const TextSectionWrapper = styled.div`
  width: 60vw;
  padding-left: 100px;
  padding-right: 50px
  border: 1px solid transparent;

  @media screen and (max-width: 767px) {
    padding: 15px !important;
    width: 100vw !important;
  }
`;

export const SubTitle = styled.div`
  font-family: Poppins-Bold;
  font-weight: bold;
  font-size: 30px;
  line-height: 150%;
  color: #3C3B3B;
  text-alin: left;

  @media screen and (max-width: 767px) {
    font-size: 20px;
    text-align: left;
    margin-top: 25px;
  }
`;

export const Description = styled.div`
  margin-top: 20px;
  font-size: 20px;
  line-height: 150%;
  color: #3C3B3B;
  font-family: IBMPlexSans;
  text-align: initial;

  @media screen and (max-width: 767px) {
    margin-top: 5px;
    font-size: 16px;
    margin-bottom: 30px;
  }
`;

export const BackgroundLines = styled.div`
  border: 1px solid #DD334D;
  border-top: 0;
  border-bottom-left-radius: 15.0125px;
  border-bottom-right-radius: 15.0125px;
  left: 250px;
  top: 0;
  width: 60%;
  height: 55%;
  position: absolute;

  @media screen and (max-width: 767px) {
    display: none;
  }
`;
