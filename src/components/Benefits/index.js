import SectionTitle from "../Shared/SectionTitle/SectionTitle.component";
import {
  Background,
  BenefitsItem,
  BenefitsRightPhotoWrapper,
  BenefitsLeftPhotoWrapper,
  Container,
  Description,
  ItemPhotoRight,
  ItemPhotoLeft,
  SubTitle,
  TextSectionWrapper,
  TitleWrapper,
  BenefitsItemRight,
  BackgroundLines,
} from "./Benefits.styles";

export default function Benefits() {
  return (
    <>
      <Container id="benefits">
        <Background />
        <TitleWrapper>
          <SectionTitle text="Benefícios" />
        </TitleWrapper>

        <BenefitsItemRight>
          <BenefitsRightPhotoWrapper>
            <ItemPhotoRight src="images/benefits/benefits-one.png" />
          </BenefitsRightPhotoWrapper>
          <TextSectionWrapper>
            <SubTitle>Agilidade para o seu processo</SubTitle>
            <Description>Com o Infovist, você elimina etapas desnecessárias em seu processo e ganha tempo pra focar no que realmente importa na sua empresa: a satisfação do seu cliente. Corte intermediários, otimize custos e mantenha controle de todo o processo de vistoria, com todas as informações ao seu alcance!</Description>
          </TextSectionWrapper>
        </BenefitsItemRight>

        <BenefitsItem>
          <BenefitsLeftPhotoWrapper>
            <ItemPhotoLeft src="images/benefits/benefits-two.png" />
          </BenefitsLeftPhotoWrapper>
          <TextSectionWrapper>
            <SubTitle>Relatórios disponíveis  de forma rápida</SubTitle>
            <Description>Reduza o tempo de espera na hora de oferecer seus serviços aos seus clientes. Com o Infovist, sua empresa ganha muito mais tempo no processo. Você envia o link da vistoria para seu cliente, ele tira e envia as fotos solicitadas e nós geramos o relatório para ajudar na sua tomada de decisão. Tudo simples, rápido e pronto para você fazer suas análises!</Description>
          </TextSectionWrapper>
        </BenefitsItem>

        <BenefitsItemRight>
          <BenefitsRightPhotoWrapper>
            <ItemPhotoRight src="images/benefits/benefits-three.png" />
          </BenefitsRightPhotoWrapper>
          <TextSectionWrapper>
            <SubTitle>Proteção contra fraudes</SubTitle>
            <Description>Aumente a segurança do seu processo! O Infovist oferece mecanismos que validam se as vistorias estão sendo realizadas pelos seus clientes ou não. Reduza o risco de fraudes e traga mais confiança para os seus clientes.</Description>
          </TextSectionWrapper>
        </BenefitsItemRight>

        <BenefitsItem>
          <BenefitsLeftPhotoWrapper>
            <ItemPhotoLeft src="images/benefits/benefits-four.png" />
          </BenefitsLeftPhotoWrapper>
          <TextSectionWrapper>
            <SubTitle>Diversos tipos de vistorias</SubTitle>
            <Description>Com o Infovist, você não fica limitado quando o assunto é vistoria. Oferecemos vistorias para motos, carros, caminhões e cargas, e estamos aumentando nosso portfolio cada vez mais para atender a sua empresa com a melhor qualidade possível.</Description>
          </TextSectionWrapper>
        </BenefitsItem>
      </Container>
    </>
  );
}
