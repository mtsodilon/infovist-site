import SectionTitle from "../Shared/SectionTitle/SectionTitle.component";
import {
  Container,
  List,
  ListItem,
  ListWrapper,
  SecurityDescription,
  SecurityDescriptionTitle,
  SecurityDescriptionWrapper,
  SecurityInfoWrapper,
  SecutiryIconWrapper,
  TitleWrapper,
} from "./DataReceive.styles";

export default function DataReceive() {
  return (
    <>
      <Container id="data">
        <TitleWrapper>
          <SectionTitle text="Quais dados você recebe?" />
        </TitleWrapper>
        <ListWrapper>
          <List>
            <ListItem>
              <img src="images/sufix.svg" />
              Dados do veículo e Precificação FIPE
            </ListItem>
            <ListItem>
              <img src="images/sufix.svg" />
              Roubos e Furtos
            </ListItem>
            <ListItem>
              <img src="images/sufix.svg" />
              Restrições do veículo
            </ListItem>
          </List>
          <List>
            <ListItem>
              <img src="images/sufix.svg" />
              Gravames
            </ListItem>
            <ListItem>
              <img src="images/sufix.svg" />
              Leilões e Sinistros
            </ListItem>
            <ListItem>
              <img src="images/sufix.svg" />
              Débitos e Multas
            </ListItem>
          </List>
        </ListWrapper>
        <SecurityInfoWrapper>
          <SecutiryIconWrapper>
            <img src="images/lock.svg" />
          </SecutiryIconWrapper>
          <SecurityDescriptionWrapper>
            <SecurityDescriptionTitle>E mais...</SecurityDescriptionTitle>
            <SecurityDescription>
              O Infovist valida a identidade do seu cliente na hora da vistoria e oferece para a sua empresa dados 100% protegidos, atualizados e seguindo todas as diretrizes da LGPD.
            </SecurityDescription>
          </SecurityDescriptionWrapper>
        </SecurityInfoWrapper>
      </Container>
    </>
  );
};