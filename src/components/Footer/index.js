import {
  Container,
  Email,
  EmailLabel,
  Logo,
  LogoWrapper,
  Column,
  Row,
  MenuItem,
  AddressTitle,
  Address,
  Unity,
  AddressColumnLeft,
  CopyrightRow,
} from "./Footer.styles";

export default function Footer(){
  return (
    <>
      <Container>
        <LogoWrapper>
          <Logo src="images/logo-white.svg" />
        </LogoWrapper>
        <Column>
          <MenuItem>Como funciona?</MenuItem>
          <MenuItem>Benefícios</MenuItem>
          <MenuItem>Quais dados você recebe</MenuItem>
          <MenuItem>Clientes</MenuItem>
        </Column>
        <div>
          <AddressTitle>Onde estamos</AddressTitle>
          <Row>
            <AddressColumnLeft>
              <Unity>Unidade 1</Unity>
              <Address>Av. Paulista, 726 - 17º andar -<br/>conj. 1707 - Bela Vista</Address>
              <Address>São Paulo - SP</Address>
              <Address>01310-910</Address>
            </AddressColumnLeft>
            <Column>
              <Unity>Unidade 2</Unity>
              <Address>R. Humaitá, 203 - Jardim Paulista</Address>
              <Address>Guarulhos - SP</Address>
              <Address>07083-150</Address>
            </Column>
          </Row>
        </div>
        <div>
          <EmailLabel>Fale com a gente</EmailLabel>
          <Email>contato@infovist.com.br</Email>
        </div>
      </Container>
      <CopyrightRow>
        Copyright © 2020 Infovist - Todos os direitos reservados
      </CopyrightRow>
    </>
  );
};
