import React from 'react';
import styled from 'styled-components';

export const Container = styled.div`
  padding: 15px 50px;
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  -webkit-box-shadow: 0 3px 5px rgba(57, 63, 72, 0.3);
  -moz-box-shadow: 0 3px 5px rgba(57, 63, 72, 0.3);
  box-shadow: 0 3px 5px rgba(57, 63, 72, 0.3);
  z-index: 9999;
  background-color: #fff;

  -webkit-transition: all 0.5s ease;
  -moz-transition: position 10s;
  -ms-transition: position 10s;
  -o-transition: position 10s;
  transition: all 0.5s ease;

  @media screen and (max-width: 767px) {
    padding: 15px 5px;
  }

  .fixed {
    position: fixed;
    top: 0;
    left: 0;
    animation: smoothScroll 1s forwards;
  }
  @keyframes smoothScroll {
    0% {
      transform: translateY(-40px);
    }
    100% {
      transform: translateY(0px);
    }
  }
`;

export const Items = styled.div`
  display: flex;
  flex-direction: row;
`;

export const HeaderItem = styled.div`
  font-size: 14px;
  line-height: 21px;
  display: flex;
  align-items: center;
  text-align: center;
  color: #3C3B3B;
`;

export const Action = styled.div`
  padding: 10px 15px;
  font-size: 14px;
  border-radius: 4px;
  color: #fff;
  background: #D40020;
  border: 2px solid #D40020;
  cursor: pointer;

  &:hover {
    opacity: 0.7;
  }
`;

export const OutlinedAction = styled.div`
  padding: 10px 15px;
  font-size: 14px;
  border-radius: 4px;
  color: #fff;
  background: #fff;
  border: 2px solid #D40020;
  color: #D40020;
  cursor: pointer;

  &:hover {
    opacity: 0.6;
  }
`;