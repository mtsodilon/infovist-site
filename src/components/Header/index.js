import {
  Action,
  Container,
  HeaderItem,
  Items,
  OutlinedAction,
} from './Header.styles.js';
import styles from '../../../styles/Home.module.css';
import { Link, animateScroll as scroll } from "react-scroll";
import { useEffect, useState } from 'react';

export default function Header() {
  const [isScroll, setIsScroll] = useState('');

  const handleScroll = () => {
    if (window.pageYOffset > 0) {
      setIsScroll('fixed');
    } else {
      setIsScroll('');
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
  }, []);

  return (
    <>
      <Container className={`${isScroll}`}>
        <img src="images/logo.svg" alt="Logo Infovist" />
        <Items className={styles.hiddeOnMobile}>
          <HeaderItem className={styles.headerMenuItem}>
            <Link
              duration={300}
              to="operation"
              smooth
            >
              Como funciona?
            </Link>
          </HeaderItem>
          <HeaderItem className={styles.headerMenuItem}>
            <Link
              duration={300}
              to="benefits"
              smooth
            >
              Benefícios
            </Link>
          </HeaderItem>
          <HeaderItem className={styles.headerMenuItem}>
            <Link
              duration={300}
              to="data"
              smooth
            >
              Quais dados você recebe
            </Link>
          </HeaderItem>
          <HeaderItem className={styles.headerMenuItem}>
            <Link
              duration={300}
              to="clients"
              smooth
            >
              Clientes
            </Link>
          </HeaderItem>
        </Items>
        <Items>
          <OutlinedAction className={styles.headerAction}>
            <Link
              to="contact"
              smooth
              duration={300}
            >
              Fale com a gente
            </Link>
          </OutlinedAction>
          <Action
            className={styles.headerAction, styles.hiddeOnMobile}
          >
            <a href="http://infovist.com.br/">
              Entrar
            </a>
          </Action>
        </Items>
      </Container>
    </>
  );
}