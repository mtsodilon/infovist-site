import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 100px;
  flex-direction: column;
`;

export const Description = styled.div`
  font-family: IBMPlexSans;
  font-size: 20px;
  line-height: 150%;
  text-align: center;
  color: #3C3B3B;
  max-width: 60vw;
  margin-top: 20px;
  @media screen and (max-width: 767px) {
    max-width: 100vw;
    padding: 0 15px;
    font-size: 16px;
  }
`;

export const PhotosWrapper = styled.div`
  display: flex;
  padding-top: 20px;

  @media screen and (max-width: 767px) {
    flex-direction: column;
  }
`;

export const SquarePhotoWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  position: relative;
  width: 35vw;
  margin: 0 20px;

  @media screen and (max-width: 767px) {
    width: 40vw;
    alingn-items: center;
    justify-content: center;
  }
`;

export const SquareFirstPhoto = styled.div`
  background: url('images/operation/square-lines-first.svg'), #DD334D;
  border-radius: 20px;
  width: 100%;
  height: 80%;
  text-align: center;
  position: absolute;
  background-repeat: no-repeat;
  background-position: center;

  @media screen and (max-width: 767px) {
  }
`;

export const SquareSecondPhoto = styled.div`
  background: url('images/operation/square-lines-second.svg'), #DD334D;
  border-radius: 20px;
  width: 100%;
  height: 80%;
  text-align: center;
  position: absolute;
  background-repeat: no-repeat;
  background-position: center;

  @media screen and (max-width: 767px) {
  }
`;

export const Photo = styled.img`
  z-index: 99;
  margin: auto;

  @media screen and (max-width: 767px) {
    width: 50vw;
  }
`;

export const SquarePhotoDescription = styled.span`
  font-family: Kalam;
  font-style: normal;
  font-weight: normal;
  font-size: 30px;
  line-height: 150%;
  text-align: center;
  color: #3C3B3B;

  @media screen and (max-width: 767px) {
    font-size: 24px;
    max-width: 40vw;
  }
`;

export const ArrowPhoto = styled.img`
  margin-top: 10px;
`;

export const Action = styled.button`
  font-family: IBMPlexSansBold;
  background: #D40020;
  border: 1.70213px solid #D40020;
  box-sizing: border-box;
  border-radius: 3.40426px;
  color: #fff;
  font-size: 18.7234px;
  line-height: 18px;
  padding: 20px;
  margin-top: 50px;
  cursor: pointer;
  &:hover {
    opacity: 0.85;
  }

  @media screen and (max-width: 767px) {
    max-width: 100vw;
    margin-left: 15px;
    margin-right: 15px;
    font-size: 14px !important;
  }
`;

export const MobileContentRight = styled.div`
  @media screen and (max-width: 767px) {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
`;

export const MobileContentLeft = styled.div`
  @media screen and (max-width: 767px) {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
  }
`;

