import SectionTitle from "../Shared/SectionTitle/SectionTitle.component";
import styles from '../../../styles/Home.module.css';
import {
  Photo,
  Container,
  Description,
  PhotosWrapper,
  SquarePhotoWrapper,
  SquarePhotoDescription,
  ArrowPhoto,
  Action,
  MobileContentRight,
  MobileContentLeft,
  SquareSecondPhoto,
  SquareFirstPhoto,
} from "./Operation.styles";

export default function Operation() {
  return (
    <>
      <Container id="operation">
        <SectionTitle text="Como Funciona?" />
        <Description>
          Você envia um link diretamente ao seu cliente ou segurado.
          Ele <strong>faz a vistoria pelo nosso aplicativo e envia as fotos na mesma hora</strong>. Após o envio, buscamos as informações das maiores bases de dados veiculares do Brasil e <strong>geramos um relatório completo</strong> para ajudar no seu processo de decisão. <strong>Tudo isso em poucas horas</strong>. Fácil, né?
        </Description>
        <PhotosWrapper>
          <MobileContentRight className={styles.textCenter}>
            <SquarePhotoWrapper>
              <Photo src="images/operation/operation-photo-first.svg" />
              <SquareFirstPhoto>
              </SquareFirstPhoto>
            </SquarePhotoWrapper>
            <div className={styles.hiddeOnMobile}>
              <ArrowPhoto src="images/operation/left-arrow.svg" />
            </div>
            <SquarePhotoDescription>Sua empresa ganha tempo.</SquarePhotoDescription>
          </MobileContentRight>

          <MobileContentLeft className={styles.textCenter}>
            <SquarePhotoWrapper>
              <Photo src="images/operation/operation-photo-second.svg" />
              <SquareSecondPhoto>
              </SquareSecondPhoto>
            </SquarePhotoWrapper>
            <div className={styles.hiddeOnMobile}>
              <ArrowPhoto src="images/operation/right-arrow.svg" />
            </div>
            <SquarePhotoDescription>Seu cliente ganha praticidade.</SquarePhotoDescription>
          </MobileContentLeft>

        </PhotosWrapper>
        <Action>Quero revolucionar meu processo de vistoria</Action>
      </Container>
    </>
  );
}