import SectionTitle from "../Shared/SectionTitle/SectionTitle.component";
import {
  Container,
  Logo,
  LogoMobileRow,
  LogosWrapper,
  TitleWrapper
} from "./Partnership.styles";

export default function Partnership() {
  return (
    <>
      <Container id="clients">
        <TitleWrapper>
          <SectionTitle text="Quem já revolucionou com a gente?" />
        </TitleWrapper>
        <LogosWrapper>
          <LogoMobileRow>
            <Logo src="images/partnership/logo-tradicao.svg" />
            <Logo src="images/partnership/logo-bancorbras.svg" />
          </LogoMobileRow>
          <LogoMobileRow>
            <Logo src="images/partnership/logo-hsconsorcios.svg" />
            <Logo src="images/partnership/logo-servopa.svg" />
          </LogoMobileRow>
          <LogoMobileRow>
            <Logo src="images/partnership/logo-fipal.svg" />
          </LogoMobileRow>
        </LogosWrapper>
      </Container>
    </>
  );
};