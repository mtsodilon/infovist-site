import styled from 'styled-components';

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: fit-content;

  @media screen and (max-width: 767px) {
    padding: 0 30px;
  }
`;

export const Title = styled.div`
  font-weight: bold;
  font-size: 38px;
  line-height: 150%;
  color: #3C3B3B;
  font-family: Poppins-Bold;

  @media screen and (max-width: 767px) {
    font-size: 26px;
    text-align: center;
  }
`;
